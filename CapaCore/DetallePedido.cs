﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaCore
{
    public class DetallePedido
    {
        private int _IdDetalle;
        private int _IdPedido;
        private int _IdProducto;
        private int _Cantidad;
        private float _PrecioVenta;
        private float _Descuento;
        private float _Importe;


        public int IdDetalle
        {
            get { return _IdDetalle; }
            set { _IdDetalle = value; }
        }

        public int IdPedido
        {
            get { return _IdPedido; }
            set { _IdPedido = value; }
        }

        public int IdProducto
        {
            get { return _IdProducto; }
            set { _IdProducto = value; }
        }

        public int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }
    }
}
