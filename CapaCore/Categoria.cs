﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaCore
{
    public class Categoria
    {
        private int _IdCategoria ;
        private string _Nombre;
        private string _Descripcion;

        public int IdCategoria
        {
            get { return _IdCategoria; }
            set { _IdCategoria = value; }
        }

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
    }
}
