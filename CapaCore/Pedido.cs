﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaCore
{
    public class Pedido
    {
        private int _IdPedido;
        private int _IdCliente;
        private int _IdEmpleado;
        private DateTime _FechaPedido;
        private float _Subtotal;
        private float _Total;
        private float _IVA;


        public int IdPedido
        {
            get { return _IdPedido; }
            set { _IdPedido = value; }
        }

        public int IdCliente
        {
            get { return _IdCliente; }
            set { _IdCliente = value; }
        }

        public int IdEmpleado
        {
            get { return _IdEmpleado; }
            set { _IdEmpleado = value; }
        }

        public DateTime FechaPedido
        {
            get { return _FechaPedido; }
            set { _FechaPedido = value; }
        }

        public float Subtotal
        {
            get { return _Subtotal; }
            set { _Subtotal = value; }
        }

        public float Total
        {
            get { return _Total; }
            set { _Total = value; }
        }

        public float IVA
        {
            get { return _IVA; }
            set { _IVA = value; }
        }
    }
}
