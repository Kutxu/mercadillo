﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient; 

namespace BDConexion
{
    public class ConexionBD
    {
        private string cadena = "Data Source=.;Initial Catalog=Mercadillo;Integrated Security=True";
        private SqlConnection conexion;
        public SqlConnection conectarBD()
        {
            conexion = new SqlConnection(cadena);
            if (conexion.State == ConnectionState.Open)
            {
                conexion.Close();
            }
            else
            {
                conexion.Open();
            }
            return conexion;
        }
    }
}
